package uk.co.wansdyke.jsonschema;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@JsonSchema(title = "Test", description = "Used to ensure our schema generation works")
public class Root {

	@NotNull
	private Date start;
	
	@Size(min = 1, max = 99)
	private int count;

	@Size(min = 1)
	private int increment;
	
	@Size(min = 1)
	private long[] customerIds;
	
	private double aDouble;
	
	private long aLong;
	
	private boolean exists;
	
	@NotNull
	private Child child;
}
