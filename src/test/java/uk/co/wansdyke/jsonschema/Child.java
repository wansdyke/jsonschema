package uk.co.wansdyke.jsonschema;

import java.util.UUID;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class Child {
	
	private UUID id;

	@Pattern(regexp="a-zA-Z0-9")
	private String string1;
	
	@NotNull
	private String string2;
	
	private String string3;
	
	private int[] values;
}
