package uk.co.wansdyke.jsonschema;

import org.junit.Test;

public class JsonSchemaTest {

	@Test
	public void mainSuccess() throws Exception {
		System.out.println(Main.process(Root.class));
	}

	@Test
	public void example() throws Exception {
		System.out.println(Main.process(Customer.class));
	}
}
