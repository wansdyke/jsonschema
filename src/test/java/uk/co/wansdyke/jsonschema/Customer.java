package uk.co.wansdyke.jsonschema;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@JsonSchema(title = "Customer", description = "Describes a Customer")
public class Customer {

	@NotNull
	private long id;
	
	@Size(min = 1, max = 250)
	private int region;
	
	@Pattern(regexp = "[a-zA-Z]+")
	private String firstname;
}
