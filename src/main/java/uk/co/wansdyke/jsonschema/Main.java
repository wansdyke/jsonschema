package uk.co.wansdyke.jsonschema;

import uk.co.wansdyke.jsonschema.schematypes.SchemaRoot;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class Main {

	public static String process(final Class<?> clazz) throws JsonProcessingException {
		final JsonSchema jsonSchema = clazz.getAnnotation(JsonSchema.class);
		
		final SchemaRoot root = new SchemaRoot(clazz);
		root.setTitle(jsonSchema.title());
		root.setDescription(jsonSchema.description());
		root.setSchema(jsonSchema.schema());
		root.setPathStart(jsonSchema.pathStart());
		
		final ObjectMapper m = new ObjectMapper();
		m.enable(SerializationFeature.INDENT_OUTPUT);
		m.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
		m.setSerializationInclusion(Include.NON_NULL);
		m.setSerializationInclusion(Include.NON_EMPTY);
		
		return m.writeValueAsString(root);
	}
}