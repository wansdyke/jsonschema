package uk.co.wansdyke.jsonschema;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface JsonSchema {
	String description() default "";
	String schema() default "http://json-schema.org/draft-04/schema#";
	String title() default "";
	String pathStart() default "";
}