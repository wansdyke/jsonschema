package uk.co.wansdyke.jsonschema.schematypes;

import java.lang.reflect.Field;

import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class SchemaNumber extends SchemaString {
	
	private Integer minimum;
	
	private Integer maximum;

	public SchemaNumber(final Field field, final Class<?> clazz) {
		super(field, clazz);
		
		setType(SchemaType.NUMBER);
		
		final Size size = field.getAnnotation(Size.class);

		if (size != null) {
			setMinimum(size.min());
			setMaximum(size.max());
		}
	}
}