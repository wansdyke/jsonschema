package uk.co.wansdyke.jsonschema.schematypes;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonPropertyOrder(value={"schema", "pathStart", "title", "description",
		"type", "properties", "required"})
public class SchemaRoot extends SchemaObject {

	@JsonProperty("$schema")
	private String schema;
	
	private String title;
	
	private String pathStart;
	
	public SchemaRoot(final Class<?> clazz) {
		super(clazz);
	}
}