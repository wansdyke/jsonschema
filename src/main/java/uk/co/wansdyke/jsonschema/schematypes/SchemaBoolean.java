package uk.co.wansdyke.jsonschema.schematypes;

import java.lang.reflect.Field;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class SchemaBoolean extends SchemaObject {

	public SchemaBoolean(final Field field, final Class<?> clazz) {
		setType(SchemaType.BOOLEAN);
	}
}
