package uk.co.wansdyke.jsonschema.schematypes;

public class SchemaDateTime extends SchemaObject {

	public SchemaDateTime(final Class<?> clazz) {
		setType(SchemaType.DATE_TIME);
	}
}