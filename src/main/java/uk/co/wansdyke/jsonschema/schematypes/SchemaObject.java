package uk.co.wansdyke.jsonschema.schematypes;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Data
@JsonPropertyOrder(value={"description", "type", "properties", "required"})
public class SchemaObject {

	private SchemaType type = SchemaType.OBJECT;
	
	private String description;
	
	private List<String> required = new ArrayList<>();
	
	private Map<String, SchemaObject> properties = new HashMap<>();
	
	protected SchemaObject() {};
	
	public SchemaObject(final Class<?> clazz) {
		final Field[] fields = clazz.getDeclaredFields();
		
		for (final Field field : fields) {
			final Class<?> child = field.getType();
			
			if (field.getAnnotationsByType(NotNull.class).length > 0) {
				required.add(field.getName());
			}
			
			if (child.isArray()) {
				getProperties().put(field.getName(), new SchemaArray(field, child));
			} else {
				switch (child.getName()) {
				case "java.util.UUID":
					getProperties().put(field.getName(), new SchemaUUID(field, child));
					break;
				case "java.util.Date":
					getProperties().put(field.getName(), new SchemaDateTime(child));
					break;
				case "java.lang.String":
					getProperties().put(field.getName(), new SchemaString(field, child));
					break;
				case "float":
				case "double":
				case "long":
					getProperties().put(field.getName(), new SchemaNumber(field, child));
					break;
				case "int":
					getProperties().put(field.getName(), new SchemaInt(field, child));
					break;
				case "boolean":
					getProperties().put(field.getName(), new SchemaBoolean(field, child));
					break;
				default:
					getProperties().put(field.getName(), new SchemaObject(child));
					break;
				}
			}
		}
	}
}
