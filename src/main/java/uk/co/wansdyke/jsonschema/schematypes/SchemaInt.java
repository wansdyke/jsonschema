package uk.co.wansdyke.jsonschema.schematypes;

import java.lang.reflect.Field;

import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class SchemaInt extends SchemaObject {
	
	private Integer minimum;
	
	private Integer maximum;

	public SchemaInt(final Field field, final Class<?> clazz) {
		setType(SchemaType.INTEGER);
		
		final Size size = field.getAnnotation(Size.class);

		if (size != null) {
			setMinimum(size.min());
			setMaximum(size.max());
		}
	}
}
