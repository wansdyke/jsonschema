package uk.co.wansdyke.jsonschema.schematypes;

import java.lang.reflect.Field;

import javax.validation.constraints.Pattern;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class SchemaString extends SchemaObject {
	
	private String pattern;

	public SchemaString(final Field field, final Class<?> clazz) {
		setType(SchemaType.STRING);
		
		final Pattern regexp = field.getAnnotation(Pattern.class);
		
		if (regexp != null) {
			pattern = regexp.regexp();
		}
	}
}