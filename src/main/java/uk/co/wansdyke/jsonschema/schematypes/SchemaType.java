package uk.co.wansdyke.jsonschema.schematypes;

public enum SchemaType {
	ARRAY,
	BOOLEAN,
	DATE_TIME,
	NUMBER,
	OBJECT,
	STRING,
	INTEGER;
	
	public String toString() {
		return name().toLowerCase().replace('_', '-');
	}
}