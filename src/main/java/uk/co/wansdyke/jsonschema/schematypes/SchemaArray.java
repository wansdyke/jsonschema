package uk.co.wansdyke.jsonschema.schematypes;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class SchemaArray extends SchemaObject {
	
	private Map<String, String> items = new HashMap<>();
	
	private Integer minItems;
	
	private Integer maxItems;

	public SchemaArray(final Field field, final Class<?> clazz) {
		setType(SchemaType.ARRAY);
		
		final Size size = field.getAnnotation(Size.class);

		if (size != null) {
			setMinItems(size.min());
			setMaxItems(size.max());
		}
		
		switch (clazz.getComponentType().getName()) {
		case "java.lang.String":
			items.put("type", SchemaType.STRING.toString());
			break;
		case "int":
			items.put("type", SchemaType.INTEGER.toString());
			break;
		case "java.util.Date":
			items.put("type", SchemaType.DATE_TIME.toString());
			break;
		case "java.util.UUID":
			items.put("type", SchemaType.STRING.toString());
			break;
		case "float":
		case "double":
		case "long":
			items.put("type", SchemaType.NUMBER.toString());
			break;
		case "boolean":
			items.put("type", SchemaType.BOOLEAN.toString());
			break;
		default:
			items.put("type", SchemaType.OBJECT.toString());
			break;
		}
	}
}