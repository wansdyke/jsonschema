package uk.co.wansdyke.jsonschema.schematypes;

import java.lang.reflect.Field;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class SchemaUUID extends SchemaString {

	public SchemaUUID(final Field field, final Class<?> clazz) {
		super(field, clazz);
		
		setDescription("java.lang.UUID");
		setPattern("^[a-fA-F0-9]{8}(-[a-fA-F0-9]{4}){3}-[a-fA-F0-9]{12}$");
	}
}