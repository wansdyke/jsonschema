# jsonschema #

This is a library for producing JSON schemas from Java model objects.

### Example ###

Annotate your class with @JsonSchema:

```
#!java

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@JsonSchema(title = "Customer", description = "Describes a Customer")
public class Customer {

	@NotNull
	private long id;
	
	@Size(min = 1, max = 250)
	private int region;
	
	@Pattern(regexp = "[a-zA-Z]+")
	private String firstname;
}
```

Call uk.co.wansdyke.jsonschema.Main.process(Customer.class) and get the following result:

```
#!javascript

{
  "$schema" : "http://json-schema.org/draft-04/schema#",
  "title" : "Customer",
  "description" : "Describes a Customer",
  "type" : "object",
  "properties" : {
    "firstname" : {
      "type" : "string",
      "pattern" : "[a-zA-Z]+"
    },
    "id" : {
      "type" : "number"
    },
    "region" : {
      "type" : "integer",
      "minimum" : 1,
      "maximum" : 250
    }
  },
  "required" : [ "id" ]
}
```


### Maven ###

There's a maven plugin available to build your schemas when you build your code:

https://bitbucket.org/wansdyke/jsonschema-maven-plugin